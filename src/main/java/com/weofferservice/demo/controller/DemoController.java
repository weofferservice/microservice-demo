package com.weofferservice.demo.controller;

import com.weofferservice.demo.config.metrics.counter.Count;
import com.weofferservice.demo.entity.Demo;
import com.weofferservice.demo.entity.DemoWithLinks;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Demo controller.
 *
 * @author Peaceful Coder
 */
@RestController
@RequestMapping(DemoController.DEMO)
@PreAuthorize("isAuthenticated()")
@Api(tags = "API Name", description = "API Description") // swagger
@RequiredArgsConstructor
public class DemoController {

	public static final String DEMO = "/demo";

	@Count
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping
	public DemoWithLinks returnWithLinks(@RequestBody @Validated Demo demo) {
		final DemoWithLinks demoWithLinks = new DemoWithLinks(demo);
		final Link selfLink = linkTo(methodOn(DemoController.class).returnWithLinks(demo)).withSelfRel();
		demoWithLinks.add(selfLink);
		final Link readAllLink = linkTo(methodOn(DemoController.class).readAll()).withRel("readAll");
		demoWithLinks.add(readAllLink);
		return demoWithLinks;
	}

	@Count
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/{uuid}")
	public DemoWithLinks read(@PathVariable @ApiParam(example = "00000000-0000-0000-0000-000000000000") UUID uuid) {
		final DemoWithLinks demoWithLinks =
				new DemoWithLinks(
						new Demo(uuid, "stringNotNullAndPseudoValidator", "stringSize", 15));
		final Link selfLink = linkTo(methodOn(DemoController.class).read(uuid)).withSelfRel();
		demoWithLinks.add(selfLink);
		return demoWithLinks;
	}

	@Count
	@GetMapping
	public CollectionModel<DemoWithLinks> readAll() {
		final List<DemoWithLinks> demoWithLinksList =
				IntStream.range(1, 6).boxed()
						.map(number -> {
							final UUID uuid = UUID.randomUUID();
							final DemoWithLinks demoWithLinks = new DemoWithLinks(
									new Demo(
											uuid,
											"stringNotNullAndPseudoValidator" + number,
											"stringSize" + number,
											number));
							final Link selfLink =
									linkTo(methodOn(DemoController.class).read(uuid)).withSelfRel();
							demoWithLinks.add(selfLink);
							return demoWithLinks;
						})
						.collect(toList());
		final Link selfLink = linkTo(methodOn(DemoController.class).readAll()).withSelfRel();
		return CollectionModel.of(demoWithLinksList, selfLink);
	}
}
