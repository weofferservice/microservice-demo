package com.weofferservice.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Alert controller.
 *
 * @author Peaceful Coder
 */
@Slf4j
@RestController
@RequestMapping(AlertController.ALERT)
public class AlertController {

	public static final String ALERT = "/alert";

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public void alert(@RequestBody String content) {
		log.warn("alert = '{}'", content);
	}
}
