package com.weofferservice.demo.config;

import com.weofferservice.demo.validation.PseudoValidator;
import org.springframework.plugin.core.Plugin;
import org.springframework.stereotype.Component;
import springfox.bean.validators.plugins.Validators;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelPropertyBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelPropertyContext;

import java.util.Optional;

/**
 * Swagger {@link Plugin} for {@link PseudoValidator} annotation.
 *
 * @author Peaceful Coder
 */
@Component
public class PseudoValidatorPlugin implements ModelPropertyBuilderPlugin {
    @Override
    public boolean supports(DocumentationType delimiter) {
        return true;
    }

    @Override
    public void apply(ModelPropertyContext context) {
        Optional<PseudoValidator> pseudoValidator = Validators.extractAnnotation(context, PseudoValidator.class);
        if (pseudoValidator.isPresent()) {
            context.getSpecificationBuilder().example("@PseudoValidator plugin is working");
        }
    }
}