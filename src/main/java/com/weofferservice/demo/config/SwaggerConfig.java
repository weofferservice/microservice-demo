package com.weofferservice.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ImplicitGrant;
import springfox.documentation.service.LoginEndpoint;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Swagger configuration.
 *
 * @author Peaceful Coder
 */
@Configuration
public class SwaggerConfig {

	private static final String SECURITY_REFERENCE = "Go to demo";

	@Autowired
	private KeycloakProperties keycloakProperties;

	@Bean
	public Docket swagger() {
		return new Docket(DocumentationType.SWAGGER_2) // for validation works correct
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.weofferservice.demo")) // only my controllers
				.build()
				.apiInfo(apiInfo())
				.securitySchemes(buildSecuritySchemes())
				.securityContexts(buildSecurityContexts());
	}

	@Bean
	public SecurityConfiguration swaggerSecurityConfiguration() {
		return SecurityConfigurationBuilder.builder()
				.clientId(keycloakProperties.getResource())
				.appName("Swagger UI for WeOfferService")
				.additionalQueryStringParams(
						Collections.singletonMap("nonce", UUID.randomUUID()))
				.build();
	}

	private List<SecurityContext> buildSecurityContexts() {
		final List<SecurityReference> securityReferences = Collections.singletonList(
				SecurityReference.builder()
						.reference(SECURITY_REFERENCE)
						.scopes(buildAuthorizationScopes().toArray(new AuthorizationScope[0]))
						.build());
		final SecurityContext context =
				SecurityContext.builder()
						.securityReferences(securityReferences)
						.build();
		return Collections.singletonList(context);
	}

	private List<SecurityScheme> buildSecuritySchemes() {
		return Collections.singletonList(new OAuth(
				SECURITY_REFERENCE,
				buildAuthorizationScopes(),
				Collections.singletonList(new ImplicitGrant(
						new LoginEndpoint(
								keycloakProperties.getAuthServerUrl() + "/realms/" + keycloakProperties.getRealm() + "/protocol/openid-connect/auth"),
						null))));
	}

	private List<AuthorizationScope> buildAuthorizationScopes() {
		return Collections.emptyList();
	}

	private ApiInfo apiInfo() {
		final Contact contact = new Contact("CONTACT-NAME", "http://www.contact-url.com", "contact@email.com");
		return new ApiInfo(
				"Title: DEMO API title",
				"Description: DEMO API description",
				"Version: DEMO API version",
				"/absolute-url",
				contact,
				"License: DEMO API license",
				"relative-url",
				Collections.emptyList());
	}
}
