package com.weofferservice.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Keycloak properties.
 *
 * @author Peaceful Coder
 */
@Data
@Component
@ConfigurationProperties(prefix = "keycloak")
public class KeycloakProperties {

	private String authServerUrl;

	private String realm;

	private String resource;
}
