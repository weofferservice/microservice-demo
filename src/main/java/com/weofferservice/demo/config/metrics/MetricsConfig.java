package com.weofferservice.demo.config.metrics;

import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import lombok.NonNull;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServlet;

/**
 * Metrics configuration.
 *
 * @author Peaceful Coder
 */
@Configuration
public class MetricsConfig {

	@Bean
	public PrometheusMeterRegistry standardMetrics(@NonNull PrometheusConfig prometheusConfig) {
		final PrometheusMeterRegistry prometheusMeterRegistry = new PrometheusMeterRegistry(prometheusConfig);
		prometheusMeterRegistry.config().meterFilter(
				MeterFilters.standardMeterFilter()
		);
		return prometheusMeterRegistry;
	}

	@Bean
	public PrometheusMeterRegistry demoMetrics(@NonNull PrometheusConfig prometheusConfig) {
		final PrometheusMeterRegistry prometheusMeterRegistry = new PrometheusMeterRegistry(prometheusConfig);
		prometheusMeterRegistry.config().meterFilter(
				MeterFilters.demoMeterFilter()
		);
		return prometheusMeterRegistry;
	}

	@Bean
	public ServletRegistrationBean<HttpServlet> standardMetricsServlet(@NonNull PrometheusMeterRegistry standardMetrics) {
		return new ServletRegistrationBean<>(
				new StandardMetricsServlet(standardMetrics.getPrometheusRegistry()),
				"/standard-metrics"
		);
	}

	@Bean
	public ServletRegistrationBean<HttpServlet> demoMetricsServlet(@NonNull PrometheusMeterRegistry demoMetrics) {
		return new ServletRegistrationBean<>(
				new DemoMetricsServlet(demoMetrics.getPrometheusRegistry()),
				"/demo-metrics"
		);
	}
}
