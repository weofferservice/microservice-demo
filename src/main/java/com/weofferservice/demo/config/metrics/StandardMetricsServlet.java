package com.weofferservice.demo.config.metrics;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.MetricsServlet;
import lombok.NonNull;

/**
 * Metrics servlet for standard-metrics.
 *
 * @author Peaceful Coder
 */
public class StandardMetricsServlet extends MetricsServlet {

	public StandardMetricsServlet(@NonNull CollectorRegistry registry) {
		super(registry);
	}
}
