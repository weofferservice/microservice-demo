package com.weofferservice.demo.config.metrics;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.MetricsServlet;
import lombok.NonNull;

/**
 * Metrics servlet for demo-metrics.
 *
 * @author Peaceful Coder
 */
public class DemoMetricsServlet extends MetricsServlet {

	public DemoMetricsServlet(@NonNull CollectorRegistry registry) {
		super(registry);
	}
}
