package com.weofferservice.demo.config.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.NonNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Demo metrics configuration.
 *
 * @author Peaceful Coder
 */
@Configuration
public class DemoMetricsConfig {

	public static final String DEMO_METRICS_PREFIX = "demo.";

	@Bean
	public Counter returnWithLinksCounter(@NonNull MeterRegistry demoMetrics) {
		return Counter.builder(DEMO_METRICS_PREFIX + "returnWithLinks").register(demoMetrics);
	}

	@Bean
	public Counter readCounter(@NonNull MeterRegistry demoMetrics) {
		return Counter.builder(DEMO_METRICS_PREFIX + "read").register(demoMetrics);
	}

	@Bean
	public Counter readAllCounter(@NonNull MeterRegistry demoMetrics) {
		return Counter.builder(DEMO_METRICS_PREFIX + "readAll").register(demoMetrics);
	}
}
