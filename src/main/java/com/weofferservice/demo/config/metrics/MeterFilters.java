package com.weofferservice.demo.config.metrics;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.core.instrument.config.MeterFilterReply;
import lombok.NonNull;
import lombok.experimental.UtilityClass;

import java.util.function.Predicate;

import static com.weofferservice.demo.config.metrics.DemoMetricsConfig.DEMO_METRICS_PREFIX;

/**
 * Meter filters.
 *
 * @author Peaceful Coder
 */
@UtilityClass
public class MeterFilters {

	public MeterFilter standardMeterFilter() {
		return new MeterFilter() {
			@Override
			public MeterFilterReply accept(@NonNull Meter.Id meterId) {
				if (!demoPredicate().test(meterId)) {
					return MeterFilterReply.ACCEPT;
				}
				return MeterFilterReply.DENY;
			}
		};
	}

	public MeterFilter demoMeterFilter() {
		return new MeterFilter() {
			@Override
			public MeterFilterReply accept(Meter.Id meterId) {
				if (demoPredicate().test(meterId)) {
					return MeterFilterReply.ACCEPT;
				}
				return MeterFilterReply.DENY;
			}
		};
	}

	private Predicate<Meter.Id> demoPredicate() {
		return meterId -> meterId.getName().startsWith(DEMO_METRICS_PREFIX);
	}
}
