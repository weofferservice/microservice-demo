package com.weofferservice.demo.config.metrics.counter;

import io.micrometer.core.instrument.Counter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

import static com.weofferservice.demo.config.metrics.DemoMetricsConfig.DEMO_METRICS_PREFIX;
import static java.util.stream.Collectors.toList;

/**
 * Counter aspect.
 *
 * @author Peaceful Coder
 */
@Aspect
@Component
@RequiredArgsConstructor
public class CounterAspect {

	private final List<Counter> counters;

	@Around("@annotation(Count)")
	public Object incrementCounter(@NonNull ProceedingJoinPoint joinPoint) throws Throwable {
		final Counter counter = resolve(joinPoint.getSignature().getName());
		counter.increment();
		return joinPoint.proceed();
	}

	private Counter resolve(@NonNull String methodName) {
		final List<Counter> methodNameCounters = counters.stream()
				.filter(counter -> Objects.equals(counter.getId().getName(), DEMO_METRICS_PREFIX + methodName))
				.collect(toList());
		final int countMethodNameCounters = methodNameCounters.size();
		if (countMethodNameCounters != 1) {
			throw new IllegalStateException(
					"Count of counter for method with name '" + methodName + "' must be 1 but was " + countMethodNameCounters);
		}
		return methodNameCounters.get(0);
	}
}
