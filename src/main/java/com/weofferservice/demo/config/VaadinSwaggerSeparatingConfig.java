package com.weofferservice.demo.config;

import com.vaadin.flow.spring.SpringServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * {@link WebMvcConfigurer} for Vaadin and Swagger separating.
 *
 * @author Peaceful Coder
 */
@Configuration
public class VaadinSwaggerSeparatingConfig implements WebMvcConfigurer {

    public static final String VAADIN_URL_MAPPING = "/vaadin/*";

    //https://github.com/vaadin/spring/issues/602
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.setOrder(Ordered.LOWEST_PRECEDENCE - 2);
    }

    @Bean
    public ServletRegistrationBean<SpringServlet> springServlet(ApplicationContext context) {
        return new ServletRegistrationBean<>(
                new SpringServlet(context, true),
                VAADIN_URL_MAPPING);
    }
}