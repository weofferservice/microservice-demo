package com.weofferservice.demo.actuator;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Features endpoint.
 *
 * @author Peaceful Coder
 */
@Component
@Endpoint(id = "features")
public class FeaturesEndpoint {
    private static Map<String, Feature> FEATURES = new ConcurrentHashMap<>();

    static {
        FEATURES.put("selector1", new Feature(true, "name1"));
        FEATURES.put("selector2", new Feature(false, "name2"));
        FEATURES.put("selector3", new Feature(true, "name3"));
        FEATURES.put("selector4", new Feature(false, "name4"));
        FEATURES.put("selector5", new Feature(false, "name5"));
    }

    @ReadOperation
    public Map<String, Feature> features() {
        return FEATURES;
    }

    @ReadOperation
    public Feature feature(@Selector String selector) {
        return FEATURES.get(selector);
    }

    private static class Feature {
        private final boolean enabled;
        private final String name;

        private Feature(boolean enabled,
                        String name) {
            this.enabled = enabled;
            this.name = name;
        }
    }
}
