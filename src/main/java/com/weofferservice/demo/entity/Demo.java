package com.weofferservice.demo.entity;

import com.weofferservice.demo.validation.PseudoValidator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * Demo.
 *
 * @author Peaceful Coder
 */
@Data
@Validated
public class Demo {
    @NotNull
    private final UUID uuid;

    @NotNull
    @PseudoValidator
    private final String stringNotNullAndPseudoValidator;

    @Size(min = 5, max = 4000)
    private final String stringSize;

    @Min(10)
    @Max(100)
    @ApiModelProperty(example = "15")
    private final Integer integerMinAndMax;
}
