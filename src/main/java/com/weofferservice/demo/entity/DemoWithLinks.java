package com.weofferservice.demo.entity;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

/**
 * Demo with links.
 *
 * @author Peaceful Coder
 */
@Data
public class DemoWithLinks extends RepresentationModel<DemoWithLinks> {
    private final Demo demo;
}
