package com.weofferservice.demo.vaadin;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

/**
 * vaadin/ui/main
 */
@Route("ui/main")
public class DemoView extends VerticalLayout {
    public DemoView() {
        add(new Label("I am the Vaadin demo"));
    }
}
