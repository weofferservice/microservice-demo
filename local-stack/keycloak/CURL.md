# admin1
curl --verbose --request POST --data "grant_type=password" --data "client_id=demo-service" --data "username=admin1" --data "password=p@ssw0rd" http://host.docker.internal:7070/auth/realms/weofferservice/protocol/openid-connect/token

# user1
curl --verbose --request POST --data "grant_type=password" --data "client_id=demo-service" --data "username=user1" --data "password=qwerty" http://host.docker.internal:7070/auth/realms/weofferservice/protocol/openid-connect/token

# Invalid user credentials
curl --verbose --request POST --data "grant_type=password" --data "client_id=demo-service" --data "username=invalid" --data "password=invalid" http://host.docker.internal:7070/auth/realms/weofferservice/protocol/openid-connect/token