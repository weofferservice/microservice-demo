# microservice-demo

For correct work you need add line

    127.0.0.1       host.docker.internal

into your 'hosts' file

### Helpful endpoints

1. Swagger: http://localhost:8080/swagger-ui/
2. Demo REST: http://localhost:8080/demo
3. Prometheus metrics:
    - http://localhost:8080/demo-metrics
    - http://localhost:8080/standard-metrics
4. Vaadin view: http://localhost:8080/vaadin/ui/main